# Vzdálené servery

Tyto příkazy umožní například nahrát lokální git repo na GitLab.

## Výpis vzdálených serverů

```
git remote -v
```

## Přidání vzdáleného serveru

```
git remote add <name> <url>
```
