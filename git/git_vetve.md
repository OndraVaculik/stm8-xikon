# Větve

Větve umožňují současně řešit několik problémů, případně opravovat chyby a nerozbít zbytek projektu. Také usnadňují kooperace mezi více vývojáři.

![branches](data/branches.png)

## Vypsání existujících větví

Je nezbytné, aby byl v projektu alespoň jeden commit.

```
git branch
```

## Vytvoření nové větve

```
git branch jmeno_vetve
```

## Zmněna aktivní větce

```
git switch jmeno_vetve
```


## Spojování větví

```
git merge jmeno_vetve
```

## Smazání nepoužívané větve

Pokud je již větev sloučená:

```
git branch -d jmeno_vetve
```

Pokud není sloučená:

```
git branch -D jmeno_vetve
```
