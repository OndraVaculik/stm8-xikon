# Git - Úvod

Při programování člověk vytváří, upravuje či maže soubory. Někdy však dojde do bodu, kdy si řekne, že by bylo prima moci se vrátit zpět, do nějakého bodu, kdy kód fungoval jinak. A právě pro tyto situace se hodí verzování a verzovací systémy.

Momentálně nejpopulárnější verzovací systém na naší planetě je `git`, jemuž se budetme v této kapitole věnovat.

![GIT TREE](data/git_tree.png)

## Pár informací

* Původní vývojář __Linus Torvalds__ - tvůrce __Linuxu__
* První vydání 7. dubna 2005
* Zkratka __VCS__ - Version Control System
* Oficiální stránka projektu `git`: https://git-scm.com/

## K čemu je git dobrý?

* Záloha
* Kooperace
* Možnost vrátit se zpět
* Vývoj alternativních větví

## Základní příkazy

Základní práci v gitu ilustruje následující obrázek:

![GIT STATES](data/git_states.png)

Náš pracovní adresář ve kterém provádíme změny znázorňuje `working directory`.

Pokud chceme změny uložit do repozitáře, je třeba napřed "označit" ty soubory, jejichž stav chceme uložit. To provedeme přesunem souborů do `staging aera`.

Ke změnám, které jsou ve `staging aera` se připojí popisek a další metadata (kontrolní součet, autor, datum, ... [ty automaticky přidá `git`]) a připojí se jako noví commit do `repository`.


![GIT CMD](data/git_cmd.png)

### Zjištění verze

Dá se použít například i k ověření, že je `git` nainstalován správně.

```
git --version
```

### Založení git repozitáře

```
git init
```

### Zjištění stavu repozitáře

```
git status
```

### Přidání souboru / souborů do staged aera

```
git add
git add.
```

### Vytvoření commitu

```
git commit
git commit -m "Povinná zpráva commitu"
```

### Seznam commitů

```
git log
git log --oneline
```

### Zobrazení detailů o commitu

```
git show commit_hash
git show db9644b
```

### Nahrání změn na server

```
git push
```

### Stáhnutí změn se serveru

```
git pull
```

### Klonování

```
git clone url_k_repozitáři
git clone https://gitlab.com/wykys/stm8-tools.git
```

### Skočit na commit

```
git checkout master
git checkout db9644b
```

## GitKraken

* GUI klient
* https://www.gitkraken.com/

## Ignorece souborů

* `.gitignore`

## Platformy pro git a mnohem více

* [GitLab](https://gitlab.com/)
* [GitHub](https://github.com/)
* [GitTea](https://gitea.io/en-us/)
* [Bitbucket](https://bitbucket.org/)

### Jejich možnosti

* Správa verzí
* Zálohy
* Issue
* Fork
* Pull request
* Automatizace
  * Nasazování kódu do produkce
  * Testování
  * Buildy
* Vydání
* Dokumentace
* Komunita

---

## __Zdroje__

* [Git Reference Manual](https://git-scm.com/docs)
