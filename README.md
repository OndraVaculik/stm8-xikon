# 📖 STM8 Xikon

✍️ Lexikon poznatků o STM8

## Obsah

* Vstupy a výstupy
* [Externí přerušení - EXTI](https://gitlab.com/wykys/stm8-xikon/-/tree/main/exti)
  * [Úvod](https://gitlab.com/wykys/stm8-xikon/-/blob/main/exti/exti_intro.md)
  * [Knihovní funkce](https://gitlab.com/wykys/stm8-xikon/-/blob/main/exti/exti_fce.md)
* [Čítače / Časovače](https://gitlab.com/wykys/stm8-xikon/-/tree/main/tim)
  * [Úvod](https://gitlab.com/wykys/stm8-xikon/-/blob/main/tim/tim_intro.md)
  * [Pulzně šířková modulace - PWM](https://gitlab.com/wykys/stm8-xikon/-/blob/main/tim/tim_pwm.md)
  * [Přerušení](https://gitlab.com/wykys/stm8-xikon/-/blob/main/tim/tim_it.md)
  * [Knihovní funkce](https://gitlab.com/wykys/stm8-xikon/-/blob/main/tim/tim_fce.md)
* [Verzování - Git](https://gitlab.com/wykys/stm8-xikon/-/tree/main/git)
  * [Úvod](https://gitlab.com/wykys/stm8-xikon/-/blob/main/git/git_intro.md)
  * [Větvení](https://gitlab.com/wykys/stm8-xikon/-/blob/main/git/git_vetve.md)
  * [Zásobník](https://gitlab.com/wykys/stm8-xikon/-/blob/main/git/git_zasobnik.md)
  * [Vzdálené servery](https://gitlab.com/wykys/stm8-xikon/-/blob/main/git/git_server.md)
  * [Ukazatel HEAD](https://gitlab.com/wykys/stm8-xikon/-/blob/main/git/git_head.md)


## Zdroje

* [Datasheet](https://www.st.com/resource/en/datasheet/stm8s208rb.pdf)
* [Programming Manual](https://www.st.com/resource/en/programming_manual/pm0044-stm8-cpu-programming-manual-stmicroelectronics.pdf)
* [Reference Manual](https://www.st.com/resource/en/reference_manual/rm0016-stm8s-series-and-stm8af-series-8bit-microcontrollers-stmicroelectronics.pdf)
* [Dev Board User Manual](https://www.st.com/resource/en/user_manual/dm00477617-stm8s208rbt6-nucleo64-board-stmicroelectronics.pdf)
* [STM8 Web](https://st.com/stm8)
* [STM8 Nucleo Web](https://www.st.com/stm8nucleo)

## Další studijní materiály

* [Sallyx](https://www.sallyx.org/sally/c/) - Jazyk C
* [Elektromyš](http://www.elektromys.eu/stm8.php) - STM8
* [Marrek Chytrosti](https://chytrosti.marrek.cz/mit.html) - STM8
