# Čítače / Časovače - Přerušení

Přerušení od časovače se využívá zejména při spouštění nějakých periodických dějů (například vyčítání stavů tlačítek). Také najde uplatnění například při měření času mezi různými událostmi. Může nám umožnit psát programy které provádějí více úloh simultánně případně díky němu můžeme i implementovat multitasking.

## Předpoklady

Tohle téma navazuje na základní poznatky s úvodu do čítačů. Také předpokládá základní znalosti o fungování přerušení.

## Zdroje přerušení

Časovače mají několik zdrojů přerušení, ne všechny čítače, mají všechny dostupné:

* Přetečení
* Porovnání s hodnotou kanálu - Output Compare
* Vnější signál - Input Capture

Pokud je přerušení povoleno a má nastavenou vhodnou prioritu, je vyvoláno nastavením příslušné vlajky. Aby bylo možné volat přerušení opakovaně je třeba vlajky čistit.

![tim status reg 1](data/tim_status_reg_1.png)

Ukázka registru, ve kterém jsou vlajky používaném přetečením pro general purpose timery.

* Vlajky jsou __nastavovány HW__, je třeba je __čistit SW__!
* Na základě vlajky je provedeno IRQ


## Ukázka použití TIM2 - přerušení při přetečení

Demonstrační příklad, který využívá přerušení generované přetečením.

```c
#include "stm8s.h"

// rutina obsluhy přerušení
INTERRUPT_HANDLER(TIM2_UPD_OVF_BRK_IRQHandler, 13)
{
    // negace výstupu
    GPIO_WriteReverse(GPIOC, GPIO_PIN_5);
    // vyčištění vlajky žádosti přeručení
    TIM2_ClearITPendingBit(TIM2_IT_UPDATE);
}

// hlavní funkce
void main(void)
{
    // nastavení pracovní frekvance na 16 MHz
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);
    // inicializace GPIO
    GPIO_Init(GPIOC, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW);
    // inicializaci čítače
    TIM2_TimeBaseInit(TIM2_PRESCALER_256, 62499);
    // povolení přeřušení při přetečení
    TIM2_ITConfig(TIM2_IT_UPDATE, ENABLE);
    // spuštění čítače
    TIM2_Cmd(ENABLE);
    // nastavení priority přerušení
    ITC_SetSoftwarePriority(ITC_IRQ_TIM2_OVF, ITC_PRIORITYLEVEL_1);
    // globální povolení přeřušení MCU
    enableInterrupts();
    // zacyklení programu
    while (1)
        ;
}
```
