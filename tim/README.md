# Čítače / Časovače

* [Čítače / Časovače](https://gitlab.com/wykys/stm8-xikon/-/tree/main/tim)
  * [Úvod](https://gitlab.com/wykys/stm8-xikon/-/blob/main/tim/tim_intro.md)
  * [Pulzně šířková modulace - PWM](https://gitlab.com/wykys/stm8-xikon/-/blob/main/tim/tim_pwm.md)
  * [Přerušení](https://gitlab.com/wykys/stm8-xikon/-/blob/main/tim/tim_it.md)
  * [Knihovní funkce](https://gitlab.com/wykys/stm8-xikon/-/blob/main/tim/tim_fce.md)
