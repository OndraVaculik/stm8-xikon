# Externí přerušení - EXTI

* [Externí přerušení - EXTI](https://gitlab.com/wykys/stm8-xikon/-/tree/main/exti)
  * [Úvod](https://gitlab.com/wykys/stm8-xikon/-/blob/main/exti/exti_intro.md)
  * [Knihovní funkce](https://gitlab.com/wykys/stm8-xikon/-/blob/main/exti/exti_fce.md)
