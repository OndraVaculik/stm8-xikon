# Externí přerušení - Úvod

Externí přerušení umožňuje detekovat změny na vstupech a jako reakci na ně vyvolat žádost o přerušení. To může v některých úlohách značně snížit nároky na strojový čas procesoru, který by jinak například ve smyčce blokujícím způsobem vstupy kontroloval. Během ušetřeného času může procesor provádět jinou činnost, nebo například být v úsporném režimu.

## Předpoklady

Tato kapitola předpokládá základní znalost jazyka C, práci se `GPIO` a povědomí o tom jak funguje přerušení.

## Nastavení externího přerušení

Externí přerušení reaguje na události, jenž se stali na vstupních `GPIO`.

Pro každý port je možné nastavit,
na co bude `EXTI` citlivé:

| Anglicky                   | Česky                    |
| -------------------------- | ------------------------ |
| Falling edge and low level | Sestupná hrana a logická 0  |
| Rising edge only           | náběžná hrana            |
| Falling edge only          | sestupná hrana           |
| Rising and falling edge    | náběžná i sestupná hrana |

Aby `EXTI` reagovalo, je nezbytné správně nakonfigurovat `GPIO`, jako vstupní s povoleným přerušením. Připadají v úvahu 2 možné konfigurace, plovoucí vstupní pin, případně vstupní pin s pull-up odporem.

```c
// plovoucí vstupní pin a povoleným EXTI
GPIO_Init(GPIOC, GPIO_PIN_3, GPIO_MODE_IN_FL_IT);
```

```c
// vstupní pin s pull-up a povoleným EXTI
GPIO_Init(GPIOC, GPIO_PIN_3, GPIO_MODE_IN_PU_IT);
```

Blokový diagram `GPIO`, se zvýrazněnými signály, pro `EXTI`.

![EXTI GPIO](data/exti_gpio_block_diagram.png)

### Zdroje externího přerušení

Pro `EXTI` je vyhrazeno 5 vektorů přerušení, pro každý port jeden. Leč ne všechny `GPIO` lze použít pro externí přerušení. Následující tabulka, převzatá s reference manualu shrnuje, které piny budou jako zdroj `EXTI` fungovat.

| Port  | Počet vstupů |  Rozsah   |
| :---: | :----------: | :-------: |
|  `A`  |      5       | `PA[6:2]` |
|  `B`  |      8       | `PB[7:0]` |
|  `C`  |      8       | `PC[7:0]` |
|  `D`  |      7       | `PD[6:0]` |
|  `E`  |      8       | `PE[7:0]` |

Pin `PD7`, případně `PC3` (v závislosti na pouzdru) může být zdrojem `TLI` (Top Level Interrupt source).

## Ukázka použití externího přerušení

```c
#include "stm8s.h"

// obslužná rutina externího přerušení
INTERRUPT_HANDLER(EXTI_PORTE_IRQHandler, 7)
{
    GPIO_WriteReverse(GPIOC, GPIO_PIN_5);
}

void main(void)
{
    // nastavení hodin na 16 MHz
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);
    // PC5 jako výstup
    GPIO_Init(GPIOC, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW);
    // PE4 vstup generující žádost o přerušení
    GPIO_Init(GPIOE, GPIO_PIN_4, GPIO_MODE_IN_FL_IT);
    // nastavení citlivosti externího přerušení přerušení
    EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOE, EXTI_SENSITIVITY_RISE_FALL);
    // nastavení priority přerušení
    ITC_SetSoftwarePriority(ITC_IRQ_PORTE, ITC_PRIORITYLEVEL_1);
    // povolení přeruření
    enableInterrupts();
    // nekonečná smyčka
    while (1)
        ;
}
```
